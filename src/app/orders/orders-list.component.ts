import {Component} from '@angular/core';

@Component({
  selector: 'pm-orders',
  templateUrl: './orders-list.component.html'
})

export class OrdersListComponent {
  pageTitle: string = 'Order List';
  listFilter: string;
  orders: any[] = [
    {
      "orderId": 1,
      "inviterId": "Leaf Rake",
      "date": "GDN-0011",
      "productName": "Leaf rake with 48-inch wooden handle.",
      "price": 19.95,
      "imageUrl": "https://openclipart.org/image/300px/svg_to_png/26215/Anonymous_Leaf_Rake.png"
    },
    {
      "orderId": 2,
      "inviterId": "Garden Cart",
      "date": "GDN-0023",
      "productName": "15 gallon capacity rolling garden cart",
      "price": 32.99,
      "imageUrl": "https://openclipart.org/image/300px/svg_to_png/58471/garden_cart.png"
    },
    {
      "orderId": 5,
      "inviterId": "Hammer",
      "date": "TBX-0048",
      "productName": "Curved claw steel hammer",
      "price": 8.9,
      "imageUrl": "https://openclipart.org/image/300px/svg_to_png/73/rejon_Hammer.png"
    }
  ];
}
